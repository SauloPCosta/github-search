//
//  NetworkHelper.swift
//  GitHubSearch
//
//  Created by Saulo Costa on 22/10/16.
//
//

import UIKit

class CustomFormatter: NSDateFormatter {
    
    override init() {
        super.init()
        self.timeZone = NSTimeZone.localTimeZone()
        self.locale = NSLocale(localeIdentifier: "pt_BR_POSIX")
        self.dateFormat = "yyyy-MM-dd'T'HH:mm:ss'Z'"
    }
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
    }

}
