//
//  UserDetailViewController.swift
//  GitHubSearch
//
//  Created by Saulo Costa on 23/10/16.
//
//

import UIKit

class UserDetailViewController: BaseViewController, UITableViewDataSource, UITableViewDelegate, UserDetailProtocol {

    // MARK: - Properties
    var presenter: UserDetailPresenter!
    var user: User?
    var repositoryList = [Repository]()
    var userLogin : String!
    

    let headerCellID = "UserDetailHeaderCell"
    let cellID = "SearchRepositoryCell"
    let loadCellID = "LoadCell"
    var haveNextPage = false
    
    // MARK: - Outlets
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setupUI()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if segue.identifier == "SegueRepositoryDetail" {
            let vc = segue.destinationViewController as! RepositoryDetailViewController
            vc.repository = sender as! Repository
        }
    }
 
    
    // MARK: - UITableViewDataSource UITableViewDelegate
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        if user == nil {
            return 0
        }
        return 1
    }
    
    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let cell = tableView.dequeueReusableCellWithIdentifier(headerCellID) as! UserDetailHeaderCell
        cell.setupCellWithItem(user!)
        return cell
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if haveNextPage {
            return repositoryList.count+1
        }
        return repositoryList.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        if haveNextPage && indexPath.row == repositoryList.count {
            let cell = tableView.dequeueReusableCellWithIdentifier(loadCellID)
            presenter.loadNextPage()
            return cell!
        }
        let cell = tableView.dequeueReusableCellWithIdentifier(cellID) as! SearchRepositoryCell
        cell.setupCellWithItem(repositoryList[indexPath.row])
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        performSegueWithIdentifier("SegueRepositoryDetail", sender: repositoryList[indexPath.row])
    }

    // MARK: - Methods
    
    func setupUI() {
        
        tableView.estimatedRowHeight = 336
        tableView.rowHeight = UITableViewAutomaticDimension
        
        tableView.estimatedSectionHeaderHeight = 280
        tableView.sectionHeaderHeight = UITableViewAutomaticDimension
        title = userLogin
        self.presenter = UserDetailPresenter(userDetailProtocol: self)
        self.presenter.getUserFullInfo(userLogin)
        
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .Plain, target: nil, action: nil)
    }
    
    // MARK: - UserDetailProtocol
    
    func updateWithResult(result: [Repository]) {
        repositoryList = result
        self.tableView.reloadData()
    }
    
    func finishLoadNextPage(nextPage: [Repository]) {
        repositoryList.appendContentsOf(nextPage)
        self.tableView.reloadData()
    }
    
    func haveNextPage(status: Bool) {
        self.haveNextPage = status
    }
    
    func popViewController() {
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    func updateWithUser(user: User) {
        self.user = user
    }
}
