//
//  SearchUserCell.swift
//  GitHubSearch
//
//  Created by Saulo Costa on 23/10/16.
//
//

import UIKit

class SearchUserCell: UITableViewCell {
    
    @IBOutlet weak var lblUserLogin: UILabel!
    @IBOutlet weak var lblScore: UILabel!
    @IBOutlet weak var imgUserAvatar: UIImageView!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        selectionStyle = .None
        imgUserAvatar.layer.borderColor = UIColor.blackColor().CGColor
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setupCellWithItem(item: User) {
        lblUserLogin.text = item.login
        lblScore.text = "Score: "+item.score.stringValue
        if !item.avatar_url.isEmpty {
            imgUserAvatar.setImageWithUrl(NSURL(string: item.avatar_url)!)
        }
    }
}
