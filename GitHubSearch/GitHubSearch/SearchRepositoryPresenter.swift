//
//  SearchRepositoryPresenter.swift
//  GitHubSearch
//
//  Created by Saulo Costa on 23/10/16.
//
//

import UIKit

class SearchRepositoryPresenter: NSObject {
    var searchRepositoryProtocol : SearchRepositoryProtocol!
    var nextPage: String?
    
    init(searchRepositoryProtocol: SearchRepositoryProtocol) {
        self.searchRepositoryProtocol = searchRepositoryProtocol
        super.init()
    }
    
    func searchWithTerm(term: String) {
        searchRepositoryProtocol.showLoader()
        RepositoryBO.searchRepository(term) { (success, list, nextPage, error) in
            if error == nil && success {
                
                self.nextPage = nextPage
                
                if list?.count == 0 {
                    self.searchRepositoryProtocol.showInfoWithStatus("No repository was found!")
                    self.searchRepositoryProtocol.updateWithResult(list!)
                }
                else {
                    self.searchRepositoryProtocol.hideLoader()
                    if nextPage == nil {
                        self.searchRepositoryProtocol.haveNextPage(false)
                    }
                    else {
                        self.searchRepositoryProtocol.haveNextPage(true)
                    }
                    self.searchRepositoryProtocol.updateWithResult(list!)
                }
            } else if error == Errors.Conection {
                self.searchRepositoryProtocol.showErrorWithStatus("Please, check your internet connection and try again!")
            }
            else {
                self.searchRepositoryProtocol.showErrorWithStatus("An error occurred while processing your request, try again!")
            }
        }
    }
    
    func loadNextPage() {
        RepositoryBO.searchRepositoryNextPage(nextPage!) { (success, list, nextPage, error) in
            if error == nil && success {
                self.searchRepositoryProtocol.hideLoader()
                self.nextPage = nextPage
                if nextPage == nil {
                    self.searchRepositoryProtocol.haveNextPage(false)
                }
                else {
                    self.searchRepositoryProtocol.haveNextPage(true)
                }
                self.searchRepositoryProtocol.finishLoadNextPage(list!)
            } else if error == Errors.Conection {
                self.searchRepositoryProtocol.showErrorWithStatus("Please, check your internet connection and try again!")
            }
            else {
                self.searchRepositoryProtocol.showErrorWithStatus("An error occurred while processing your request, try again!")
            }
        }
    }
}
