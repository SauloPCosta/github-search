//
//  SearchUserProtocol.swift
//  GitHubSearch
//
//  Created by Saulo Costa on 23/10/16.
//
//

import UIKit

protocol SearchUserProtocol: BaseProtocol {
    func updateWithResult(result: [User])
    func finishLoadNextPage(nextPage: [User])
    func haveNextPage(status: Bool)
}
