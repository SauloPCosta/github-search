//
//  UserDAO.swift
//  GitHubSearch
//
//  Created by Saulo Costa on 23/10/16.
//
//

import UIKit

class UserDAO: NSObject {

    class func searchUser(term: String, completion: (success: Bool, list:[User]?, nextPage:String?, error:NSError?) -> Void) {
        let endPoint = "/search/users?q="+term
        
        RequestHelper.requestWithEndpoint(endPoint, metod: RequestMethod.GET, timeout: Constants.defaultTimeout, body: nil, paginated: false) { (responseError, response, responseData) in
            if (responseError == nil) {
                if let httpResponse = response as? NSHTTPURLResponse {
                    if httpResponse.statusCode == 200 {
                        var nextPage:String?
                        var itemList = [User]()
                        if let linkUnwrap = httpResponse.allHeaderFields["Link"] as? String {
                            nextPage = self.getNextPage(linkUnwrap)
                        }
                        let responseDict = Utils.convertDataToDictionary(responseData)
                        if responseDict != nil {
                            if let items = responseDict!["items"] as? [[String:AnyObject]] {
                                for objDict in items {
                                    let user = User.init(dictionary: objDict)
                                    itemList.append(user)
                                }
                            }
                        }
                        completion(success: true, list: itemList, nextPage: nextPage, error: nil)
                    }else {completion(success: false,list: nil, nextPage: nil, error: Errors.Generic)}
                } else {completion(success: false,list: nil, nextPage: nil, error: Errors.Generic)}
            }else {completion(success: false,list: nil, nextPage: nil, error: responseError)}
        }
    }
    
    class func fullUserInfo(user: String, completion: (success: Bool, user:User?, error:NSError?) -> Void) {
        let endPoint = "/users/"+user
        
        RequestHelper.requestWithEndpoint(endPoint, metod: RequestMethod.GET, timeout: Constants.defaultTimeout, body: nil, paginated: false) { (responseError, response, responseData) in
            if (responseError == nil) {
                if let httpResponse = response as? NSHTTPURLResponse {
                    if httpResponse.statusCode == 200 {
                        var user : User?
                        
                        let responseDict = Utils.convertDataToDictionary(responseData)
                        if responseDict != nil {
                            user = User.init(dictionary: responseDict!)
                        }
                        completion(success: true, user: user, error: nil)
                    }else {completion(success: false,user: nil, error: Errors.Generic)}
                } else {completion(success: false,user: nil, error: Errors.Generic)}
            }else {completion(success: false,user: nil, error: responseError)}
        }
    }
    
    class func searchNextPage(page: String, completion: (success: Bool, list:[User]?, nextPage:String?, error:NSError?) -> Void) {
        
        RequestHelper.requestWithEndpoint(page, metod: RequestMethod.GET, timeout: Constants.defaultTimeout, body: nil, paginated: true) { (responseError, response, responseData) in
            if (responseError == nil) {
                if let httpResponse = response as? NSHTTPURLResponse {
                    if httpResponse.statusCode == 200 {
                        var nextPage:String?
                        var itemList = [User]()
                        if let linkUnwrap = httpResponse.allHeaderFields["Link"] as? String {
                            nextPage = self.getNextPage(linkUnwrap)
                        }
                        let responseDict = Utils.convertDataToDictionary(responseData)
                        if responseDict != nil {
                            if let items = responseDict!["items"] as? [[String:AnyObject]] {
                                for objDict in items {
                                    let user = User.init(dictionary: objDict)
                                    itemList.append(user)
                                }
                            }
                        }
                        completion(success: true, list: itemList, nextPage: nextPage, error: nil)
                    }else {completion(success: false,list: nil, nextPage: nil, error: Errors.Generic)}
                } else {completion(success: false,list: nil, nextPage: nil, error: Errors.Generic)}
            }else {completion(success: false,list: nil, nextPage: nil, error: responseError)}
        }
    }
    
    class func getNextPage(header: String) -> String? {
        let headerArray = header.characters.split{$0 == ","}.map(String.init)
        var nextPage = ""
        var lastPage = ""
        
        for string in headerArray {
            if string.containsString("rel=\"next\"") {
                let startRange: Range<String.Index> = string.rangeOfString("<")!
                let finalRange: Range<String.Index> = string.rangeOfString(">")!
                let range = startRange.endIndex...finalRange.startIndex.advancedBy(-1)
                nextPage = string.substringWithRange(range)
            }
            if string.containsString("rel=\"last\"") {
                let startRange: Range<String.Index> = string.rangeOfString("<")!
                let finalRange: Range<String.Index> = string.rangeOfString(">")!
                let range = startRange.endIndex...finalRange.startIndex.advancedBy(-1)
                lastPage = string.substringWithRange(range)
            }
        }
        if !nextPage.isEmpty && !lastPage.isEmpty && nextPage != lastPage{
            return nextPage
        }
        return nil
    }
}
