//
//  SearchRepositoryCell.swift
//  GitHubSearch
//
//  Created by Saulo Costa on 23/10/16.
//
//

import UIKit

class SearchRepositoryCell: UITableViewCell {

    
    @IBOutlet weak var lblRepositoryName: UILabel!
    @IBOutlet weak var imgRepositoryOwner: UIImageView!
    @IBOutlet weak var lblRepositoryOwnerName: UILabel!
    
    @IBOutlet weak var lblWatch: UILabel!
    @IBOutlet weak var lblStar: UILabel!
    @IBOutlet weak var lblFork: UILabel!
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var lblLastUpdate: UILabel!
    @IBOutlet weak var lblLanguage: UILabel!
    @IBOutlet weak var lblScore: UILabel!
    @IBOutlet weak var lblCreationDate: UILabel!
    
    @IBOutlet weak var viewOwner: UIView!
    @IBOutlet weak var viewWatch: UIView!
    @IBOutlet weak var viewStar: UIView!
    @IBOutlet weak var viewFork: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        selectionStyle = .None
        viewFork.layer.borderColor = UIColor.lightGrayColor().CGColor
        viewWatch.layer.borderColor = UIColor.lightGrayColor().CGColor
        viewStar.layer.borderColor = UIColor.lightGrayColor().CGColor
        viewOwner.layer.borderColor = Utils.hexStringToUIColor("c9e6f2").CGColor
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setupCellWithItem(item: Repository) {
        lblRepositoryName.text = item.name
        
        if !(item.owner?.avatar_url.isEmpty)! {
            imgRepositoryOwner.setImageWithUrl(NSURL(string: (item.owner?.avatar_url)!)!)
        }
        lblRepositoryOwnerName.text = item.owner?.login
        lblWatch.text = item.watchers_count.stringValue
        lblStar.text = item.stargazers_count.stringValue
        lblFork.text = item.forks_count.stringValue
        
        lblDescription.text = item.repositoryDescription
        lblLanguage.text = "Language: "+item.language
        lblScore.text = "Score: "+item.score.stringValue
        
        let formatter = CustomFormatter()
        formatter.dateFormat = "dd/MM/yyyy"
        
        lblCreationDate.text = "Created: "+formatter.stringFromDate(item.created_at)
        lblLastUpdate.text = "Last update: "+formatter.stringFromDate(item.updated_at)
    }
}
