//
//  SearchUserViewController.swift
//  GitHubSearch
//
//  Created by Saulo Costa on 23/10/16.
//
//

import UIKit

class SearchUserViewController: BaseViewController, UITableViewDataSource, UITableViewDelegate, UISearchBarDelegate, UISearchResultsUpdating, UIScrollViewDelegate, SearchUserProtocol {
    
    // MARK: - Properties
    var presenter: SearchUserPresenter!
    var userList = [User]()
    let searchController = UISearchController(searchResultsController: nil)
    let cellID = "SearchUserCell"
    let loadCellID = "LoadCell"
    let emptyCellID = "EmptyCell"
    var haveNextPage = false
    
    
    // MARK: - Outlets
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setupUI()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBarHidden = true
    }
    
    override func viewWillDisappear(animated: Bool)
    {
        super.viewWillDisappear(animated)
        self.navigationController?.navigationBarHidden = false
    }
    

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if segue.identifier == "SegueUserdetail" {
            let vc = segue.destinationViewController as! UserDetailViewController
            vc.userLogin = sender as! String
        }
    }
    
    
    // MARK: - UITableViewDataSource UITableViewDelegate
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if userList.count == 0 {
            return 1
        }
        if haveNextPage {
            return userList.count+1
        }
        return userList.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        if haveNextPage && indexPath.row == userList.count {
            let cell = tableView.dequeueReusableCellWithIdentifier(loadCellID)
            presenter.loadNextPage()
            return cell!
        }
        if userList.count == 0 {
            let cell = tableView.dequeueReusableCellWithIdentifier(emptyCellID)
            cell?.selectionStyle = .None
            return cell!
        }
        let cell = tableView.dequeueReusableCellWithIdentifier(cellID) as! SearchUserCell
        cell.setupCellWithItem(userList[indexPath.row])
        return cell
    }
    
    func scrollViewDidScroll(scrollView: UIScrollView) {
        searchController.searchBar.resignFirstResponder()
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if haveNextPage && indexPath.row == userList.count ||  userList.count == 0{
            return
        }
        self.performSegueWithIdentifier("SegueUserdetail", sender: userList[indexPath.row].login)
    }
    
    // MARK: - UISearchBarDelegate UISearchResultsUpdating
    
    func updateSearchResultsForSearchController(searchController: UISearchController) {
        
    }
    
    func searchBarCancelButtonClicked(searchBar: UISearchBar) {
        
    }
    
    func searchBarSearchButtonClicked(searchBar: UISearchBar) {
        if searchBar.text != nil {
            self.presenter.searchWithTerm(searchBar.text!)
        }
    }
    
    // MARK: - Methods
    
    func setupUI() {
        searchController.searchResultsUpdater = self
        searchController.searchBar.delegate = self
        searchController.dimsBackgroundDuringPresentation = false
        searchController.hidesNavigationBarDuringPresentation = false
        definesPresentationContext = true
        tableView.tableHeaderView = searchController.searchBar
        self.extendedLayoutIncludesOpaqueBars = true
        
        searchController.searchBar.backgroundColor = UIColor.whiteColor()
        searchController.searchBar.tintColor = Colors.kGitHubBlack
        
        tableView.estimatedRowHeight = 80
        tableView.rowHeight = UITableViewAutomaticDimension
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .Plain, target: nil, action: nil)
        self.presenter = SearchUserPresenter(searchUserProtocol: self)
    }
    
    // MARK: - SearchUserProtocol
    
    func updateWithResult(result: [User]) {
        userList = result
        self.tableView.reloadData()
    }

    func finishLoadNextPage(nextPage: [User]) {
        userList.appendContentsOf(nextPage)
        self.tableView.reloadData()
    }
    
    func haveNextPage(status: Bool) {
        self.haveNextPage = status
    }
}
