//
//  UserDetailPresenter.swift
//  GitHubSearch
//
//  Created by Saulo Costa on 23/10/16.
//
//

import UIKit

class UserDetailPresenter: NSObject {
    var userDetailProtocol : UserDetailProtocol!
    var nextPage: String?
    
    init(userDetailProtocol: UserDetailProtocol) {
        self.userDetailProtocol = userDetailProtocol
        super.init()
    }
    
    func getUserFullInfo(user: String) {
        userDetailProtocol.showLoader()
        UserBO.fullUserInfo(user) { (success, user, error) in
            if error == nil && success {
                self.userDetailProtocol.updateWithUser(user!)
                self.searchWithTerm((user?.login)!)
            } else if error == Errors.Conection {
                self.userDetailProtocol.showErrorWithStatus("Please, check your internet connection and try again!")
                self.userDetailProtocol.popViewController()
            }
            else {
                self.userDetailProtocol.showErrorWithStatus("An error occurred while processing your request, try again!")
                self.userDetailProtocol.popViewController()
            }
        }
    }
    
    func searchWithTerm(term: String) {
        RepositoryBO.searchRepositoryFromUser(term) { (success, list, nextPage, error) in
            if error == nil && success {
                self.userDetailProtocol.hideLoader()
                self.nextPage = nextPage
                
                if list == nil {
                    self.userDetailProtocol.showInfoWithStatus("The user has no repository!")
                }
                else {
                    if nextPage == nil {
                        self.userDetailProtocol.haveNextPage(false)
                    }
                    else {
                        self.userDetailProtocol.haveNextPage(true)
                    }
                    self.userDetailProtocol.updateWithResult(list!)
                }
            } else if error == Errors.Conection {
                self.userDetailProtocol.showErrorWithStatus("Please, check your internet connection and try again!")
                self.userDetailProtocol.popViewController()
            }
            else {
                self.userDetailProtocol.showErrorWithStatus("An error occurred while processing your request, try again!")
                self.userDetailProtocol.popViewController()
            }
        }
    }
    
    func loadNextPage() {
        RepositoryBO.searchRepositoryNextPage(nextPage!) { (success, list, nextPage, error) in
            if error == nil && success {
                self.userDetailProtocol.hideLoader()
                self.nextPage = nextPage
                if nextPage == nil {
                    self.userDetailProtocol.haveNextPage(false)
                }
                else {
                    self.userDetailProtocol.haveNextPage(true)
                }
                self.userDetailProtocol.finishLoadNextPage(list!)
            } else if error == Errors.Conection {
                self.userDetailProtocol.showErrorWithStatus("Please, check your internet connection and try again!")
                self.userDetailProtocol.popViewController()
            }
            else {
                self.userDetailProtocol.showErrorWithStatus("An error occurred while processing your request, try again!")
                self.userDetailProtocol.popViewController()
            }
        }
    }
}
