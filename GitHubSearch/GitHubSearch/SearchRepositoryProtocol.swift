//
//  SearchRepositoryProtocol.swift
//  GitHubSearch
//
//  Created by Saulo Costa on 23/10/16.
//
//

import UIKit

protocol SearchRepositoryProtocol: BaseProtocol {
    func updateWithResult(result: [Repository])
    func finishLoadNextPage(nextPage: [Repository])
    func haveNextPage(status: Bool)
}
