//
//  Constants.swift
//  GitHubSearch
//
//  Created by Saulo Costa on 22/10/16.
//
//

import UIKit

class Constants: NSObject {
    class var defaultTimeout:NSTimeInterval {
        return 30
    }
}

enum Errors {
    static let Generic = NSError(domain: "Generic", code: -100, userInfo: [NSLocalizedDescriptionKey: "Generic error"])
    static let Conection = NSError(domain: "NoConnection", code: -99, userInfo: [NSLocalizedDescriptionKey: "No Internet connection"])
    static let BadServerResponse = NSError(domain: "BadServerResponse", code: -98, userInfo: [NSLocalizedDescriptionKey: "Bad server response"])
}

enum RequestMethod {
    static let GET = "GET"
    static let POST = "POST"
    static let PUT = "PUT"
    static let DELETE = "DELETE"
}

struct Colors {
    static let kGitHubBlack = UIColor.init(red: 0.200, green: 0.200, blue: 0.200, alpha: 1)
}