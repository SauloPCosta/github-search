//
//  RepositoryDetailViewController.swift
//  GitHubSearch
//
//  Created by Saulo Costa on 23/10/16.
//
//

import UIKit

class RepositoryDetailViewController: BaseViewController {
    
    // MARK: - Properties
    var repository : Repository!
    
    // MARK: - Outlets
    @IBOutlet weak var lblRepositoryName: UILabel!
    @IBOutlet weak var imgRepositoryOwner: UIImageView!
    @IBOutlet weak var lblRepositoryOwnerName: UILabel!
    
    @IBOutlet weak var lblWatch: UILabel!
    @IBOutlet weak var lblStar: UILabel!
    @IBOutlet weak var lblFork: UILabel!
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var lblLastUpdate: UILabel!
    @IBOutlet weak var lblLanguage: UILabel!
    @IBOutlet weak var lblDefaultBranch: UILabel!
    @IBOutlet weak var lblCreationDate: UILabel!
    
    @IBOutlet weak var viewOwner: UIView!
    @IBOutlet weak var viewWatch: UIView!
    @IBOutlet weak var viewStar: UIView!
    @IBOutlet weak var viewFork: UIView!
    
    @IBOutlet weak var lblIssues: UILabel!
    @IBOutlet weak var lblPublic: UILabel!
    @IBOutlet weak var lblSize: UILabel!
    
    @IBOutlet weak var lblUrl: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setupUI()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    // MARK: - Methods
    
    func setupUI() {
        viewFork.layer.borderColor = UIColor.lightGrayColor().CGColor
        viewWatch.layer.borderColor = UIColor.lightGrayColor().CGColor
        viewStar.layer.borderColor = UIColor.lightGrayColor().CGColor
        viewOwner.layer.borderColor = Utils.hexStringToUIColor("c9e6f2").CGColor
        
        title = repository.full_name
        lblRepositoryName.text = repository.name
        if !(repository.owner?.avatar_url.isEmpty)! {
            imgRepositoryOwner.setImageWithUrl(NSURL(string: (repository.owner?.avatar_url)!)!)
        }
        lblRepositoryOwnerName.text = repository.owner?.login
        lblWatch.text = repository.watchers_count.stringValue
        lblStar.text = repository.stargazers_count.stringValue
        lblFork.text = repository.forks_count.stringValue
        
        lblDescription.text = repository.repositoryDescription
        lblLanguage.text = "Language: "+repository.language
        lblDefaultBranch.text = "Default branch: "+repository.default_branch
        
        let formatter = CustomFormatter()
        formatter.dateFormat = "dd/MM/yyyy"
        
        lblCreationDate.text = "Created: "+formatter.stringFromDate(repository.created_at)
        lblLastUpdate.text = "Last update: "+formatter.stringFromDate(repository.updated_at)
        
        lblIssues.text = "Issues: "+repository.open_issues_count.stringValue
        lblSize.text = repository.size.stringValue
        
        if repository.isPrivate {
            lblPublic.text = "NO"
            lblPublic.textColor = UIColor.redColor()
        }
        
        lblUrl.text = repository.html_url
    }
    
    // MARK: - Actions
    
    @IBAction func openRepositoryUrl(sender: AnyObject) {
        UIApplication.sharedApplication().openURL(NSURL(string: repository.html_url)!)
    }

}
