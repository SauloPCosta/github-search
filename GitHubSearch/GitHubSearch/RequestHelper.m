//
//  RequestHelper.m
//
//  Created by Saulo Costa.
//

#import "RequestHelper.h"
#import <UIKit/UIKit.h>

@implementation RequestHelper

+ (void)requestWithEndpoint:(NSString *)endPoint
                      metod:(NSString *)metod
                    timeout:(NSTimeInterval)timeout
                       body:(NSData*)body
                  paginated:(BOOL)paginated
                   complite:(void (^)(NSError *error,NSURLResponse *response,NSData *data))handler {
    
    NSString *baseUrl = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"API-URL"];
    if (paginated) {
        baseUrl = @"";
    }
    NSString *url = [NSString stringWithFormat:@"%@%@",baseUrl,endPoint];
    
    NSMutableURLRequest *request =
    [NSMutableURLRequest requestWithURL:[NSURL URLWithString:url]
                            cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData
                        timeoutInterval:timeout];
    [request setHTTPMethod:metod];
    if (body) {
        [request setHTTPBody:body];
    }
    else {
        NSString *encodedUrl = [url stringByAddingPercentEncodingWithAllowedCharacters:NSCharacterSet.URLQueryAllowedCharacterSet];
        [request setURL:[NSURL URLWithString:encodedUrl]];
    }
    
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
    [[[NSURLSession sharedSession] dataTaskWithRequest:request completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
        dispatch_async (dispatch_get_main_queue(), ^{
            handler(error,response,data);
        });
    }]resume];
}

@end
