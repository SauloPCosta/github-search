//
//  RepositoryBO.swift
//  GitHubSearch
//
//  Created by Saulo Costa on 23/10/16.
//
//

import UIKit

class RepositoryBO: NSObject {
    
    class func searchRepository(term: String, completion: (success: Bool,list:[Repository]?, nextPage:String?, error:NSError?) -> Void) {
        if NetworkHelper.connectedToNetwork() {
            RepositoryDAO.searchRepository(term, completion: { (success, list, nextPage, error) in
                completion(success: success,list: list, nextPage: nextPage, error: error)
            })
        }
        else {
            completion(success: false,list: nil, nextPage: nil, error: Errors.Conection)
        }
    }
    
    class func searchRepositoryFromUser(user: String, completion: (success: Bool,list:[Repository]?, nextPage:String?, error:NSError?) -> Void) {
        if NetworkHelper.connectedToNetwork() {
            RepositoryDAO.searchRepositoryFromUser(user, completion: { (success, list, nextPage, error) in
                completion(success: success,list: list, nextPage: nextPage, error: error)
            })
        }
        else {
            completion(success: false,list: nil, nextPage: nil, error: Errors.Conection)
        }
    }
    
    class func searchRepositoryNextPage(nextPage: String, completion: (success: Bool,list:[Repository]?, nextPage:String?, error:NSError?) -> Void) {
        if NetworkHelper.connectedToNetwork() {
            RepositoryDAO.searchRepositoryNextPage(nextPage, completion: { (success, list, nextPage, error) in
                completion(success: success,list: list, nextPage: nextPage, error: error)
            })
        }
        else {
            completion(success: false,list: nil, nextPage: nil, error: Errors.Conection)
        }
    }
}
