//
//  UserDetailHeaderCell.swift
//  GitHubSearch
//
//  Created by Saulo Costa on 23/10/16.
//
//

import UIKit

class UserDetailHeaderCell: UITableViewCell {

    @IBOutlet weak var imgUser: UIImageView!
    
    @IBOutlet weak var lblUserLogin: UILabel!
    
    @IBOutlet weak var lblUserLocation: UILabel!
    @IBOutlet weak var lblUserSite: UILabel!
    @IBOutlet weak var lblUserEmail: UILabel!
    
    @IBOutlet weak var lblUserName: UILabel!
    
    @IBOutlet weak var lblUserBio: UILabel!
    
    @IBOutlet weak var lblScore: UILabel!
    @IBOutlet weak var lblFollowers: UILabel!
    @IBOutlet weak var lblFollowing: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setupCellWithItem(item: User) {
        lblUserLogin.text = item.login
        if !item.avatar_url.isEmpty {
            imgUser.setImageWithUrl(NSURL(string: item.avatar_url)!)
        }
        if !item.location.isEmpty {
            lblUserLocation.text = item.location
        }
        if !item.blog.isEmpty {
            lblUserSite.text = item.blog
        }
        if !item.email.isEmpty {
            lblUserEmail.text = item.email
        }
        if !item.name.isEmpty {
            lblUserName.text = "Name: "+item.name
        }
        if !item.bio.isEmpty {
            lblUserBio.text = item.bio
        }
        lblFollowers.text = "Followers: "+item.followers.stringValue
        lblFollowing.text = "Following: "+item.following.stringValue
        lblScore.text = "Public repositories: "+item.public_repos.stringValue
    }
}
