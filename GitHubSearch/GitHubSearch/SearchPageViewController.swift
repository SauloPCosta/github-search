//
//  SearchPageViewController.swift
//  GitHubSearch
//
//  Created by Saulo Costa on 22/10/16.
//
//

import UIKit

class SearchPageViewController: UIPageViewController, UIPageViewControllerDataSource, UIPageViewControllerDelegate {

    //@IBOutlet weak var scPageSelect: UISegmentedControl!
    
    var controllers = [UIViewController]()
    var pageIndex = 0
    var vc1 : UIViewController!
    var vc2 : UIViewController!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setupUI()
        self.dataSource = self
        self.delegate = self
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    // MARK: - Methods
    
    func setupUI() {
        vc1 = UIStoryboard(name: "SearchUser", bundle: nil).instantiateInitialViewController()!
        vc1.view.tag = 0
        vc2 = UIStoryboard(name: "SearchRepository", bundle: nil).instantiateInitialViewController()!
        vc2.view.tag = 1
        
        self.setViewControllers([viewControllerAtIndex(0)], direction: UIPageViewControllerNavigationDirection.Forward, animated: false, completion: nil)
    }

     // MARK: - UIPageViewControllerDelegate
    
    func pageViewController(pageViewController: UIPageViewController, viewControllerBeforeViewController viewController: UIViewController) -> UIViewController?
    {
        if (viewController == vc2)
        {
            return viewControllerAtIndex(0)
        }
        return nil
    }
    
    func pageViewController(pageViewController: UIPageViewController, viewControllerAfterViewController viewController: UIViewController) -> UIViewController?
    {
        if (viewController == vc1)
        {
            return viewControllerAtIndex(1)
        }
        return nil
    }
    
    func viewControllerAtIndex(index: Int) -> UIViewController! {
        if index == 0 {
            return vc1
            
        }
        if index == 1 {
            
            return vc2
        }
        
        return nil
    }
    func pageViewController(pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        //self.scPageSelect.selectedSegmentIndex = pageViewController.viewControllers!.first!.view.tag
    }
    
    // MARK: - Actions
    
    @IBAction func changePage(sender: UISegmentedControl) {
        var direction = UIPageViewControllerNavigationDirection.Forward
        if sender.selectedSegmentIndex > self.viewControllers!.first!.view.tag {
            direction = UIPageViewControllerNavigationDirection.Forward
        }
        else {
            direction = UIPageViewControllerNavigationDirection.Reverse
        }
        self.setViewControllers([viewControllerAtIndex(sender.selectedSegmentIndex)], direction: direction, animated: true, completion: nil)
    }
    
    
}
