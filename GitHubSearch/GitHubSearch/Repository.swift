//
//  Repository.swift
//  GitHubSearch
//
//  Created by Saulo Costa on 22/10/16.
//
//

import UIKit

class Repository: NSObject {
    var id = NSNumber()
    var name = ""
    var full_name = ""
    var owner : User?
    var isPrivate = false
    var html_url = ""
    var repositoryDescription = ""
    var fork = false
    var url = ""
    var created_at = NSDate()
    var updated_at = NSDate()
    var pushed_at = NSDate()
    var homepage = ""
    var size = NSNumber()
    var stargazers_count = NSNumber()
    var watchers_count = NSNumber()
    var language = ""
    var forks_count = NSNumber()
    var open_issues_count = NSNumber()
    var master_branch = ""
    var default_branch = ""
    var score = NSNumber()
    
    
    init(dictionary: [String:AnyObject]) {
        
        if let idUnwrap = dictionary["id"] as? NSNumber {
            id = idUnwrap
        }
        if let nameUnwrap = dictionary["name"] as? String {
            name = nameUnwrap
        }
        if let full_nameUnwrap = dictionary["full_name"] as? String {
            full_name = full_nameUnwrap
        }
        if let ownerUnwrap = dictionary["owner"] as? [String:AnyObject] {
            owner = User(dictionary:ownerUnwrap)
        }
        if let isPrivateUnwrap = dictionary["private"] as? Bool {
            isPrivate = isPrivateUnwrap
        }
        if let html_urlUnwrap = dictionary["html_url"] as? String {
            html_url = html_urlUnwrap
        }
        if let repositoryDescriptionUnwrap = dictionary["description"] as? String {
            repositoryDescription = repositoryDescriptionUnwrap
        }
        if let forkUnwrap = dictionary["fork"] as? Bool {
            fork = forkUnwrap
        }
        if let urlUnwrap = dictionary["url"] as? String {
            url = urlUnwrap
        }

        let formatter = CustomFormatter()
        if let created_atUnwrap = dictionary["created_at"] as? String {
            created_at = formatter.dateFromString(created_atUnwrap)!
        }
        if let updated_atUnwrap = dictionary["updated_at"] as? String {
            updated_at = formatter.dateFromString(updated_atUnwrap)!
        }
        if let pushed_atUnwrap = dictionary["pushed_at"] as? String {
            pushed_at = formatter.dateFromString(pushed_atUnwrap)!
        }
        if let homepageUnwrap = dictionary["homepage"] as? String {
            homepage = homepageUnwrap
        }
        if let sizeUnwrap = dictionary["size"] as? NSNumber {
            size = sizeUnwrap
        }
        if let stargazers_countUnwrap = dictionary["stargazers_count"] as? NSNumber {
            stargazers_count = stargazers_countUnwrap
        }
        if let watchers_countUnwrap = dictionary["watchers_count"] as? NSNumber {
            watchers_count = watchers_countUnwrap
        }
        if let languageUnwrap = dictionary["language"] as? String {
            language = languageUnwrap
        }
        if let forks_countUnwrap = dictionary["forks_count"] as? NSNumber {
            forks_count = forks_countUnwrap
        }
        if let open_issues_countUnwrap = dictionary["open_issues_count"] as? NSNumber {
            open_issues_count = open_issues_countUnwrap
        }
        if let master_branchUnwrap = dictionary["master_branch"] as? String {
            master_branch = master_branchUnwrap
        }
        if let default_branchUnwrap = dictionary["default_branch"] as? String {
            default_branch = default_branchUnwrap
        }
        if let scoreUnwrap = dictionary["score"] as? NSNumber {
            score = scoreUnwrap
        }
        
        super.init()
    }
}
