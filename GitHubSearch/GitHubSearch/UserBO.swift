//
//  UserBO.swift
//  GitHubSearch
//
//  Created by Saulo Costa on 23/10/16.
//
//

import UIKit

class UserBO: NSObject {

    class func searchUser(term: String, completion: (success: Bool,list:[User]?, nextPage:String?, error:NSError?) -> Void) {
        if NetworkHelper.connectedToNetwork() {
            UserDAO.searchUser(term, completion: { (success, list, nextPage, error) in
                completion(success: success,list: list, nextPage: nextPage, error: error)
            })
        }
        else {
            completion(success: false,list: nil, nextPage: nil, error: Errors.Conection)
        }
    }
    
    class func fullUserInfo(user: String, completion: (success: Bool,user:User?, error:NSError?) -> Void) {
        if NetworkHelper.connectedToNetwork() {
            UserDAO.fullUserInfo(user, completion: { (success, user, error) in
                completion(success: success,user: user, error: error)
            })
        }
        else {
            completion(success: false,user: nil, error: Errors.Conection)
        }
    }
    
    class func searchUserNextPage(nextPage: String, completion: (success: Bool,list:[User]?, nextPage:String?, error:NSError?) -> Void) {
        if NetworkHelper.connectedToNetwork() {
            UserDAO.searchNextPage(nextPage, completion: { (success, list, nextPage, error) in
                completion(success: success,list: list, nextPage: nextPage, error: error)
            })
        }
        else {
            completion(success: false,list: nil, nextPage: nil, error: Errors.Conection)
        }
    }
    
}
