//
//  SearchUserPresenter.swift
//  GitHubSearch
//
//  Created by Saulo Costa on 23/10/16.
//
//

import UIKit

class SearchUserPresenter: NSObject {
    var searchUserProtocol : SearchUserProtocol!
    var nextPage: String?
    
    init(searchUserProtocol: SearchUserProtocol) {
        self.searchUserProtocol = searchUserProtocol
        super.init()
    }
    
    func searchWithTerm(term: String) {
        searchUserProtocol.showLoader()
        UserBO.searchUser(term) { (success, list, nextPage, error) in
            if error == nil && success {
                
                self.nextPage = nextPage
                
                if list?.count == 0 {
                    self.searchUserProtocol.showInfoWithStatus("No user was found!")
                    self.searchUserProtocol.updateWithResult(list!)
                }
                else {
                    self.searchUserProtocol.hideLoader()
                    if nextPage == nil {
                        self.searchUserProtocol.haveNextPage(false)
                    }
                    else {
                        self.searchUserProtocol.haveNextPage(true)
                    }
                    self.searchUserProtocol.updateWithResult(list!)
                }
            } else if error == Errors.Conection {
                self.searchUserProtocol.showErrorWithStatus("Please, check your internet connection and try again!")
            }
            else {
                self.searchUserProtocol.showErrorWithStatus("An error occurred while processing your request, try again!")
            }
        }
    }
    
    func loadNextPage() {
        UserBO.searchUserNextPage(nextPage!) { (success, list, nextPage, error) in
            if error == nil && success {
                self.searchUserProtocol.hideLoader()
                self.nextPage = nextPage
                if nextPage == nil {
                    self.searchUserProtocol.haveNextPage(false)
                }
                else {
                    self.searchUserProtocol.haveNextPage(true)
                }
                self.searchUserProtocol.finishLoadNextPage(list!)
            } else if error == Errors.Conection {
                self.searchUserProtocol.showErrorWithStatus("Please, check your internet connection and try again!")
            }
            else {
                self.searchUserProtocol.showErrorWithStatus("An error occurred while processing your request, try again!")
            }
        }
    }
}
