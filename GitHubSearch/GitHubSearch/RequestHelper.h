//
//  RequestHelper.h
//
//  Created by Saulo Costa.

#import <Foundation/Foundation.h>

@interface RequestHelper : NSObject

+ (void)requestWithEndpoint:(NSString *)endPoint
                      metod:(NSString *)metod
                    timeout:(NSTimeInterval)timeout
                       body:(NSData*)body
                  paginated:(BOOL)paginated
                   complite:(void (^)(NSError *error, NSURLResponse *response,NSData *data))handler;
@end
