//
//  BaseProtocol.swift
//  GitHubSearch
//
//  Created by Saulo Costa on 22/10/16.
//
//

import Foundation

protocol BaseProtocol {
    func showLoader()
    func hideLoader()
    func showWithStatus(status: String)
    func showErrorWithStatus(status: String)
    func showSuccessWithStatus(status: String)
    func showInfoWithStatus(status: String)
}
