//
//  PagerViewController.swift
//  GitHubSearch
//
//  Created by Saulo Costa on 23/10/16.
//
//

import UIKit

class PagerViewController: UIViewController, UIPageViewControllerDelegate, UIPageViewControllerDataSource {

    // MARK: - Outlets
    @IBOutlet weak var viewPager: UIView!
    @IBOutlet weak var viewUserTab: UIView!
    @IBOutlet weak var viewUserTabIndicator: UIView!
    
    @IBOutlet weak var viewRepositoryTab: UIView!
    @IBOutlet weak var viewRepositoryTabIndicator: UIView!
    
    // MARK: - Properties
    var pageViewController: UIPageViewController!
    var vc1 : UIViewController!
    var vc2 : UIViewController!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        
        
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        setupUI()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    // MARK: - UIPageViewControllerDelegate
    
    func pageViewController(pageViewController: UIPageViewController, viewControllerBeforeViewController viewController: UIViewController) -> UIViewController?
    {
        if (viewController == vc2)
        {
            return viewControllerAtIndex(0)
        }
        return nil
    }
    
    func pageViewController(pageViewController: UIPageViewController, viewControllerAfterViewController viewController: UIViewController) -> UIViewController?
    {
        if (viewController == vc1)
        {
            return viewControllerAtIndex(1)
        }
        return nil
    }
    
    func viewControllerAtIndex(index: Int) -> UIViewController! {
        if index == 0 {
            return vc1
            
        }
        if index == 1 {
            
            return vc2
        }
        
        return nil
    }
    func pageViewController(pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        selectTab(pageViewController.viewControllers!.first!.view.tag)
    }
    
    // MARK: - Methods
    
    func setupUI() {
        self.pageViewController = UIPageViewController(transitionStyle: .Scroll, navigationOrientation: .Horizontal, options: nil)
        vc1 = UIStoryboard(name: "SearchUser", bundle: nil).instantiateInitialViewController()!
        vc1.view.tag = 0
        vc2 = UIStoryboard(name: "SearchRepository", bundle: nil).instantiateInitialViewController()!
        vc2.view.tag = 1
        
        viewRepositoryTab.layer.borderColor = UIColor.lightGrayColor().CGColor
        viewUserTab.layer.borderColor = UIColor.lightGrayColor().CGColor
        
        selectTab(0)
        
        self.pageViewController.dataSource = self
        self.pageViewController.delegate = self
        self.addChildViewController(self.pageViewController)
        self.viewPager .addSubview(self.pageViewController.view)
        pageViewController.setViewControllers([viewControllerAtIndex(0)], direction: UIPageViewControllerNavigationDirection.Forward, animated: false, completion: nil)
    }
    
    func selectTab(tabIndex: Int) {
        UIView.animateWithDuration(0.3) {
            if tabIndex == 0 {
                self.viewRepositoryTab.alpha = 0.5
                self.viewRepositoryTabIndicator.alpha = 0
                self.viewRepositoryTab.layer.borderWidth = 0
                
                self.viewUserTab.alpha = 1
                self.viewUserTabIndicator.alpha = 1
                self.viewUserTab.layer.borderWidth = 1
            }
            else {
                self.viewUserTab.alpha = 0.5
                self.viewUserTabIndicator.alpha = 0
                self.viewUserTab.layer.borderWidth = 0
                
                self.viewRepositoryTab.alpha = 1
                self.viewRepositoryTabIndicator.alpha = 1
                self.viewRepositoryTab.layer.borderWidth = 1
            }
        }
    }
    
    // MARK: - Actions
    
    @IBAction func changeTab(sender: UIButton) {
        self.selectTab(sender.tag)
        var direction = UIPageViewControllerNavigationDirection.Forward
        if sender.tag > self.pageViewController.viewControllers!.first!.view.tag {
            direction = UIPageViewControllerNavigationDirection.Forward
        }
        else {
            direction = UIPageViewControllerNavigationDirection.Reverse
        }
        self.pageViewController.setViewControllers([viewControllerAtIndex(sender.tag)], direction: direction, animated: true, completion: nil)
    }
    

}
