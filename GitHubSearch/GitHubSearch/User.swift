//
//  User.swift
//  GitHubSearch
//
//  Created by Saulo Costa on 22/10/16.
//
//

import UIKit

class User: NSObject {
    var login = ""
    var id = NSNumber()
    var avatar_url = ""
    var gravatar_id = ""
    var url = ""
    var html_url = ""
    var followers_url = ""
    var following_url = ""
    var gists_url = ""
    var starred_url = ""
    var subscriptions_url = ""
    var organizations_url = ""
    var repos_url = ""
    var events_url = ""
    var received_events_url = ""
    var type = ""
    var site_admin = ""
    var name = ""
    var company = ""
    var blog = ""
    var location = ""
    var email = ""
    var hireable = ""
    var bio = ""
    var public_repos = NSNumber()
    var public_gists = NSNumber()
    var followers = NSNumber()
    var following = NSNumber()
    var created_at  = NSDate()
    var updated_at  = NSDate()
    var score = NSNumber()
    
    init(dictionary: [String:AnyObject]) {
        
        if let loginU = dictionary["login"] as? String {
            login = loginU
        }
        if let idU = dictionary["id"] as? NSNumber {
            id = idU
        }
        if let avatar_urlU = dictionary["avatar_url"] as? String {
            avatar_url = avatar_urlU
        }
        if let gravatar_idU = dictionary["gravatar_id"] as? String {
            gravatar_id = gravatar_idU
        }
        if let urlU = dictionary["url"] as? String {
            url = urlU
        }
        if let html_urlU = dictionary["html_url"] as? String {
            html_url = html_urlU
        }
        if let followers_urlU = dictionary["followers_url"] as? String {
            followers_url = followers_urlU
        }
        if let following_urlU = dictionary["following_url"] as? String {
            following_url = following_urlU
        }
        if let gists_urlU = dictionary["gists_url"] as? String {
            gists_url = gists_urlU
        }
        if let starred_urlU = dictionary["starred_url"] as? String {
            starred_url = starred_urlU
        }
        if let subscriptions_urlU = dictionary["subscriptions_url"] as? String {
            subscriptions_url = subscriptions_urlU
        }
        if let organizations_urlU = dictionary["organizations_url"] as? String {
            organizations_url = organizations_urlU
        }
        if let repos_urlU = dictionary["repos_url"] as? String {
            repos_url = repos_urlU
        }
        if let events_urlU = dictionary["events_url"] as? String {
            events_url = events_urlU
        }
        if let received_events_urlU = dictionary["received_events_url"] as? String {
            received_events_url = received_events_urlU
        }
        if let typeU = dictionary["type"] as? String {
            type = typeU
        }
        if let site_adminU = dictionary["site_admin"] as? String {
            site_admin = site_adminU
        }
        if let nameU = dictionary["name"] as? String {
            name = nameU
        }
        if let companyU = dictionary["company"] as? String {
            company = companyU
        }
        if let blogU = dictionary["blog"] as? String {
            blog = blogU
        }
        if let locationU = dictionary["location"] as? String {
            location = locationU
        }
        if let emailU = dictionary["email"] as? String {
            email = emailU
        }
        if let hireableU = dictionary["hireable"] as? String {
            hireable = hireableU
        }
        if let bioU = dictionary["bio"] as? String {
            bio = bioU
        }
        if let public_reposU = dictionary["public_repos"] as? NSNumber {
            public_repos = public_reposU
        }
        if let public_gistsU = dictionary["public_gists"] as? NSNumber {
            public_gists = public_gistsU
        }
        if let followersU = dictionary["followers"] as? NSNumber {
            followers = followersU
        }
        if let followingU = dictionary["following"] as? NSNumber {
            following = followingU
        }
        let formatter = CustomFormatter()
        if let created_atU = dictionary["created_at"] as? String {
            created_at = formatter.dateFromString(created_atU)!
        }
        if let updated_atU = dictionary["updated_at"] as? String {
            updated_at = formatter.dateFromString(updated_atU)!
        }
        if let scoreU = dictionary["score"] as? NSNumber {
            score = scoreU
        }
    super.init()
    }
}
