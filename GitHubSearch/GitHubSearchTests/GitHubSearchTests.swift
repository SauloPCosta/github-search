//
//  GitHubSearchTests.swift
//  GitHubSearchTests
//
//  Created by Saulo Costa on 22/10/16.
//
//

import XCTest
@testable import GitHubSearch

class GitHubSearchTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measureBlock {
            // Put the code you want to measure the time of here.
        }
    }
    
    func testUserSearch()  {
        UserBO.searchUser("Facebook") { (success, list, nextPage, error) in
            if error == nil && success {
                if list?.count > 0 {
                    XCTAssert(true)
                }
                else  {
                    XCTAssert(false, "There is a lot of users with this term, if the search came with no result, something went wrong!")
                }
            }
            else if error == Errors.Conection {
                //we have to ensure to make the test with internet connection
                XCTAssert(false, "NO INTERNET CONNECTION")
            }
            else {
                XCTAssert(false, error!.localizedDescription)
            }
        }
    }
    
    func testRepositorySearch() {
        RepositoryBO.searchRepository("Awesome iOS") { (success, list, nextPage, error) in
            if error == nil && success {
                if list?.count > 0 {
                    XCTAssert(true)
                }
                else  {
                    XCTAssert(false, "There is a lot of repository with this term, if the search came with no result, something went wrong!")
                }
            }
            else if error == Errors.Conection {
                //we have to ensure to make the test with internet connection
                XCTAssert(false, "NO INTERNET CONNECTION")
            }
            else {
                XCTAssert(false, error!.localizedDescription)
            }
        }
    }
    
    func testUserRepository() {
        RepositoryBO.searchRepositoryFromUser("facebook") { (success, list, nextPage, error) in
            if error == nil && success {
                if list?.count > 0 {
                    XCTAssert(true)
                }
                else  {
                    XCTAssert(false, "this user have repositories, the list cant be empty")
                }
            }
            else if error == Errors.Conection {
                //we have to ensure to make the test with internet connection
                XCTAssert(false, "NO INTERNET CONNECTION")
            }
            else {
                XCTAssert(false, error!.localizedDescription)
            }
        }
    }
    
    func testFullUserInfo() {
        UserBO.fullUserInfo("facebook") { (success, user, error) in
            if error == nil && success {
                if user != nil {
                    XCTAssert(true)
                }
                else  {
                    XCTAssert(false, "this user cant be nil")
                }
            }
            else if error == Errors.Conection {
                //we have to ensure to make the test with internet connection
                XCTAssert(false, "NO INTERNET CONNECTION")
            }
            else {
                XCTAssert(false, error!.localizedDescription)
            }
        }
    }
}
